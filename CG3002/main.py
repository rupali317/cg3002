import djikstra as dj
import re
import utils
import time
import whereAmI as wt
from math import sqrt
import json, requests

def getShortestPath(mapVar, initialNode, finalNode):
    table_location = utils.build_location_table(mapVar)
    print (table_location)
    print ("")
    print ("")
    table_neighbour = utils.build_neighbour_table(mapVar)
    print (table_neighbour)
    print ("")
    print ("")
    graph = utils.build_graph(table_location, table_neighbour)
    print (graph)
    print ("")
    print ("")
    return dj.shortestPath(graph,initialNode,finalNode)

mainUrl = "http://showmyway.comp.nus.edu.sg/getMapInfo.php"
inputMap1 = {'building' : "COM1", "level" : str(2)}
inputMap2 = {'building' : "COM1", "level" : str(3)}
inputMap3 = {'building' : "COM2", "level" : str(2)}
inputMap4 = {'building' : "COM2", "level" : str(3)}

params1 = dict(
    Building= inputMap1['building'],
    Level= inputMap1['level']
)

params2 = dict(
    Building= inputMap2['building'],
    Level= inputMap2['level']
)

params3 = dict(
    Building= inputMap3['building'],
    Level= inputMap3['level']
)
params4 = dict(
    Building= inputMap4['building'],
    Level= inputMap4['level']
)

dataArray = []

try:
    resp1 = requests.get(url=mainUrl, params=params1)
    #resp2 = requests.get(url=mainUrl, params=params2)
    resp3 = requests.get(url=mainUrl, params=params3)
    resp4 = requests.get(url=mainUrl, params=params4)
    dataArray.append({(inputMap1['building'] + inputMap1['level']) : utils.mapifyNodeIDs(json.loads(resp1.text), inputMap1['building'], inputMap1['level'])})
    #dataArray.append((inputMap2['building'] + inputMap2['level']) : utils.mapifyNodeIDs(json.loads(resp2.text), inputMap2['building'], inputMap2['level']))
    dataArray.append({(inputMap3['building'] + inputMap3['level']) : utils.mapifyNodeIDs(json.loads(resp3.text), inputMap3['building'], inputMap3['level'])})
    dataArray.append({(inputMap4['building'] + inputMap4['level']) : utils.mapifyNodeIDs(json.loads(resp4.text), inputMap4['building'], inputMap4['level'])})
except:
    print ("{0}Level{1}.json".format(inputMap1['building'], inputMap1['level']))
    with open("{0}Level{1}.json".format(inputMap1['building'], inputMap1['level'])) as data_file:
        dataArray.append({(inputMap1['building'] + inputMap1['level']) : utils.mapifyNodeIDs(json.load(data_file), inputMap1['building'], inputMap1['level'])})

    #with open("{0}Level{1}.json".format(inputMap2['building'], inputMap2['level'])) as data_file2:
        #dataArray.append((inputMap2['building'] + inputMap2['level']) : utils.mapifyNodeIDs(json.load(data_file2), inputMap2['building'], inputMap2['level']))

    with open("{0}Level{1}.json".format(inputMap3['building'], inputMap3['level'])) as data_file3:
        dataArray.append({(inputMap3['building'] + inputMap3['level']) : utils.mapifyNodeIDs(json.load(data_file3), inputMap3['building'], inputMap3['level'])})

    with open("{0}Level{1}.json".format(inputMap4['building'], inputMap4['level'])) as data_file4:
        dataArray.append({(inputMap4['building'] + inputMap4['level']) : utils.mapifyNodeIDs(json.load(data_file4), inputMap4['building'], inputMap4['level'])})

#result = dj.parse(data)
def PrintPathDictionary(path, Map):
    pathDictionary = {}
    for p in path:
        for m in Map:
            if p == m['nodeId']:
                print(str(p) + " : " + str(m['nodeName']))

initialNode = input("Enter Initial Node: ")
finalNode = input("Enter final Node: ")
data = utils.getValueFromArrayOfDicts((utils.getCOMBuildingFromNode(initialNode) + utils.getLevelFromNode(initialNode)), dataArray)
finalData = utils.getValueFromArrayOfDicts((utils.getCOMBuildingFromNode(finalNode) + utils.getLevelFromNode(finalNode)), dataArray)
path = []

if(not utils.isFromSameMap(initialNode, finalNode)):
    TONodeBuilding = utils.getCOMBuildingFromNode(finalNode)
    TONodeLevel = utils.getLevelFromNode(finalNode)
    TONode = utils.findTONodeFromMap(data['map'], TONodeBuilding, TONodeLevel)
    firstNodeNextMap = utils.getNextMapFirstNodeFromTONode(data['map'], TONode)
    #adds the com number and level before the node id
    print (firstNodeNextMap)
    #gives the number of the com building
    COMNumber = (re.findall('\d+', utils.getCOMBuildingFromNode(finalNode)))[0]
    mapifiedfirstNodeNextMap =  COMNumber + utils.getLevelFromNode(finalNode) + firstNodeNextMap
    print (mapifiedfirstNodeNextMap)
    path.append({(utils.getCOMBuildingFromNode(initialNode) + utils.getLevelFromNode(initialNode)) : getShortestPath(data['map'], initialNode, TONode)})
    path.append({(utils.getCOMBuildingFromNode(finalNode) + utils.getLevelFromNode(finalNode)) : getShortestPath(finalData['map'], mapifiedfirstNodeNextMap, finalNode)})
else:
    path.append({(utils.getCOMBuildingFromNode(initialNode) + utils.getLevelFromNode(initialNode)) : getShortestPath(data['map'], initialNode, finalNode)})

path = list(path[0].values())[0]
print (path)
PrintPathDictionary(path, data['map'])

#main algorithm
offset = 360 - wt.getMapNorthAt(data)
givenMagValue = 131;
givenX = 3
givenY = 2400
print (offset)
print ("data")
print (data)
accArray = []
magArray = []
wt.magnetometerValues2 = [givenMagValue] + wt.magnetometerValues2 #adding the first mag value to the beginning of the array
accVal = 0
magVal = 1
currNodeCrossed = wt.findNearestNode(path, givenX, givenY, data)
totalDistance = 0
netDistanceInDir = 0
currPoint = {'x': givenX, 'y': givenY}
approachingCheck = False #checks whether close to checkpoint
#we always travel to the next node that comes after the closest node to the current position
while(True):
    currNodeCrossedX = wt.getNodeX(currNodeCrossed,data)
    currNodeCrossedY = wt.getNodeY(currNodeCrossed,data)
    stepTaken = False
    nextCheckpoint = wt.getNextCheckpoint(path, currNodeCrossed)
    #print "nextCheckpoint" + str(nextCheckpoint)
    if(nextCheckpoint == wt.LAST_CONST):
        print ("You've reached your destination. You are a hero")
        break
    nextCheckpointX = wt.getNodeX(nextCheckpoint,data)
    print ("nextCheckpointX : " + str(nextCheckpointX))
    nextCheckpointY = wt.getNodeY(nextCheckpoint, data)
    print ("nextCheckpointY : " + str(nextCheckpointY))
    intendedMag = wt.getIntendedMagAngle(currNodeCrossed, nextCheckpoint, offset, data)
    #print "intendedMag" + str(intendedMag)
    time.sleep(0.5)
    if(accVal < len(wt.accelerationX)):
        wt.addAccToArray(accArray, wt.accelerationX[accVal])
        #uncomment the next line to make distance calculation work on the first pass
        #wt.addAccToArray(accArray, wt.accelerationX[accVal])
        #print accArray
        accVal += 1
    else:
        break
    if(magVal < len(wt.magnetometerValues2)):
        wt.addMagToArray(magArray, wt.magnetometerValues2[magVal])
        magVal += 1
        #print magArray
    else:
        break
    if(len(accArray) > 1):
        if(abs(accArray[-1] - accArray[-2]) >= 0.2):
            stepTaken = True
    if(len(magArray) > 0):
        currPoint = wt.findCurrentPoint(currPoint['x'], currPoint['y'], magArray[-1], stepTaken, offset)
        print ("CurrentX : "  + str(currPoint['x']) + "CurrentY : " + str(currPoint['y']))
        print ("Distance from checkpoint : " +
        str(wt.distanceBetweenTwoCoordinates(currPoint['x'], currPoint['y'], nextCheckpointX, nextCheckpointY)))
        #netDistanceInDir = wt.netDistanceInIntendedDirection(intendedMag, magArray[-1],
            #netDistanceInDir, stepTaken) #checks net distance in intended direction
        #print netDistanceInDir
        #time.sleep(1000)
        if(approachingCheck == False):
            wt.provideDeviationAngleInfo(intendedMag, magArray[-1])
        if(wt.aboutToReach(currPoint['x'], currPoint['y'], nextCheckpointX, nextCheckpointY)):
            print ("hello blah hello")
            print ("Checkpoint reached blah")
            approachingCheck = True
            wt.provideNavInstruction(path, currNodeCrossed, nextCheckpoint,
                currPoint['x'], currPoint['y'], data, offset)
            nextNextCheckpoint = wt.getNextCheckpoint(path, nextCheckpoint)
            if(nextNextCheckpoint == wt.LAST_CONST):
                print ("You are a hero")
                break
            nextNextCheckpointX = wt.getNodeX(nextNextCheckpoint, data)
            nextNextCheckpointY = wt.getNodeY(nextNextCheckpoint, data)
            currToNextNextDist = wt.distanceBetweenTwoCoordinates(currPoint['x'], currPoint['y'], nextNextCheckpointX, nextNextCheckpointY)
            nextToNextNextDist = wt.distanceBetweenTwoCoordinates(nextCheckpointX, nextCheckpointY, nextNextCheckpointX, nextNextCheckpointY)
            print ("currToNextNextDist : " + str(currToNextNextDist))
            print ("nextToNextNextDist : " + str(nextToNextNextDist))
            if(currToNextNextDist < nextToNextNextDist):
                #netDistanceInDir = 0
                approachingCheck = False
                currPoint = {'x' : nextCheckpointX, 'y' : nextCheckpointY}
                currNodeCrossed = nextCheckpoint #currNode becomes the nextnode
                print ("New currNode : " + str(currNodeCrossed))
